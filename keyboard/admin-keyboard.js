const kb = require('./keyboard-buttons.js')

module.exports = {
    home: [
        [kb.home.view]
    ],
    counter: [
        [kb.view.create, kb.view.input],
        [kb.view.show],
        [kb.back]
    ]
}