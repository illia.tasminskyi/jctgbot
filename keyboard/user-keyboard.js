const kb = require('./keyboard-buttons.js')

module.exports = {
    home: [
        [kb.home.view]
    ],
    counter: [
        [kb.view.input],
        [kb.back]
    ]
}