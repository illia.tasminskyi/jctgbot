# Встановлення бота на телефон:

### Встановити Termux: https://drive.google.com/file/d/131V4xo1y1ODI2sBYrnGPfEpuT6iiQN0O/view?usp=drive_link

### Відкрити Termux і запустити команду:
`nano cjc.sh`

### Вставте код:
```sh
#!/bin/bash
yes | pkg update --yes
yes | pkg upgrade --yes
pkg install nodejs-lts --yes
pkg install git --yes
git clone https://gitlab.com/illia.tasminskyi/jctgbot.git
echo -e '#!/bin/bash\n
cd jctgbot/\n
git pull\n
npm install\n
npm start' > ./jc.sh
chmod +x ./jc.sh
cd jctgbot/
npm install
npm start
```

### Збережіть файл:
`Ctrl + X`
`Написніть y`
`ENTER`

### Запустіть команди:
`chmod +x cjc.sh`
`./cjc.sh`

<br><br>

# Запуск бота:

### Запустіть команду:
`./jc.sh`

### Зупинити сервер бота:
Натиснути: `Ctrl + C`