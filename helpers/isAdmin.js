const User = require('../models/User.js')

const isAdmin = async userId => {
    const user = await User.findOne({ userId });
    return user.isAdmin
}

module.exports = isAdmin