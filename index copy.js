const TelegramBot = require('node-telegram-bot-api');
const mongoose = require('mongoose');
const User = require('./models/User.js')
const Password = require('./models/Password.js')
const View = require('./models/View.js')
const kb = require('./keyboard/keyboard-buttons.js')
const userKeyboard = require('./keyboard/user-keyboard.js')
const adminKeyboard = require('./keyboard/admin-keyboard.js')

require('dotenv').config()

mongoose.connect(process.env.DB_URL, {
    tls: true,
    tlsAllowInvalidCertificates: false,
}).then(() => console.log('Connected to MongoDB'))
    .catch(err => console.error(`MongoDB connection error: ${err}`));

const bot = new TelegramBot(process.env.TOKEN, { polling: true });

bot.on('message', async (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    const user = await User.findOne({ userId });

    switch (msg.text) {
        case kb.home.view:
            bot.sendMessage(chatId, 'Оберіть команду:', {
                reply_markup: {
                    resize_keyboard: true,
                    one_time_keyboard: false,
                    keyboard: user.isAdmin ? adminKeyboard.counter : userKeyboard.counter
                }
            })
            break;
        case kb.view.create:
            newPassword(chatId, user);
            break;
        case kb.view.input:
            inputPassword(chatId, user);
            break;
        case kb.view.show:
            showPassword(chatId, user);
            break;
        case kb.back:
            bot.sendMessage(chatId, 'Оберіть команду:', {
                reply_markup: {
                    resize_keyboard: true,
                    one_time_keyboard: false,
                    keyboard: user.isAdmin ? adminKeyboard.home : userKeyboard.home
                }
            })
            break

    }
});

bot.onText(/\/start/, async (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    const username = msg.from.username;
    const firstName = msg.from.first_name;
    const lastName = msg.from.last_name;

    try {
        const user = await User.findOne({ userId });
        if (!user) {
            const newUser = new User({
                userId,
                username,
                firstName,
                lastName,
            });
            await newUser.save();
            bot.sendMessage(chatId, 'Ваші дані збережено.');
        } else {
            bot.sendMessage(chatId, 'Ви вже зареєстровані.');
        }
        bot.sendMessage(chatId, 'Виберіть команду:', {
            reply_markup: {
                resize_keyboard: true,
                one_time_keyboard: false,
                keyboard: user.isAdmin ? adminKeyboard.home : userKeyboard.home
            }
        })
    }
    catch (error) {
        console.error('Error saving user to MongoDB:', error);
        bot.sendMessage(chatId, 'Сталася помилка під час збереження ваших даних.');
    }
});

// bot.onText(/\/newpassword/, async (msg) => {
//     const chatId = msg.chat.id;
//     const userId = msg.from.id;

//     try {
//         const user = await User.findOne({ userId });
//         if (!user || !user.isAdmin) {
//             bot.sendMessage(chatId, 'Ви не є адміністратором. Доступ заборонено.');
//             return;
//         }

//         bot.sendMessage(chatId, 'Введіть новий пароль:').then(() => {
//             bot.on('text', async (msg) => {
//                 const newPassword = msg.text;

//                 try {
//                     const newPasswordEntry = new Password({
//                         userId,
//                         password: newPassword,
//                     });

//                     await newPasswordEntry.save();
//                     bot.sendMessage(chatId, 'Пароль збережено.');
//                 } catch (error) {
//                     console.error('Error saving password to MongoDB:', error);
//                     bot.sendMessage(chatId, 'Помилка при збереженні пароля.');
//                 }
//             });
//         });
//     } catch (error) {
//         console.error('Error checking admin status:', error);
//         bot.sendMessage(chatId, 'Сталася помилка під час перевірки статусу адміністратора.');
//     }
// });
bot.onText(/\/newpassword/, async (msg) => {

    const chatId = msg.chat.id;
    const userId = msg.from.id;

    try {

        const user = await User.findOne({ userId });

        if (!user || !user.isAdmin) {
            bot.sendMessage(chatId, 'Ви не є адміністратором. Доступ заборонено.');
            return;
        }

        bot.sendMessage(chatId, 'Введіть новий пароль:');

        const newPasswordHandler = async (msg) => {
            if (msg.from.id !== userId) {
                return;
            }

            const newPassword = msg.text;

            try {
                const newPasswordEntry = new Password({
                    userId,
                    password: newPassword
                });

                await newPasswordEntry.save();

                bot.sendMessage(chatId, 'Пароль збережено.');

            } catch (error) {
                console.error('Error saving password:', error);
                bot.sendMessage(chatId, 'Помилка збереження пароля!');
            }

            bot.off('text', newPasswordHandler);
        };

        bot.on('text', newPasswordHandler);

    } catch (error) {
        console.error('Error checking admin status:', error);
        bot.sendMessage(chatId, 'Помилка перевірки прав адміністратора!');
    }

});



function newPassword(chatId, user) {

    try {
        // const user = await User.findOne?({ userId });
        if (!user || !user.isAdmin) {
            bot.sendMessage(chatId, 'Ви не є адміністратором. Доступ заборонено.');
            return;
        }

        bot.sendMessage(chatId, 'Введіть новий пароль:').then(() => {
            bot.on('text', async (msg) => {
                const newPassword = msg.text;

                try {
                    const newPasswordEntry = new Password({
                        userId: user.userId,
                        password: newPassword,
                    });

                    await newPasswordEntry.save();
                    bot.sendMessage(chatId, 'Пароль збережено.');
                } catch (error) {
                    console.error('Error saving password to MongoDB:', error);
                    bot.sendMessage(chatId, 'Помилка при збереженні пароля.');
                }
            });
        });
    } catch (error) {
        console.error('Error checking admin status:', error);
        bot.sendMessage(chatId, 'Сталася помилка під час перевірки статусу адміністратора.');
    }
};

bot.onText(/\/password/, async (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;

    try {
        const user = await User.findOne({ userId });
        if (!user || !user.isAdmin) {
            bot.sendMessage(chatId, 'Ви не є адміністратором. Доступ заборонено.');
            return;
        }

        const lastPasswordEntry = await Password.findOne().sort({ createdAt: -1 });

        if (lastPasswordEntry) {
            bot.sendMessage(chatId, `Останній збережений пароль: ${lastPasswordEntry.password}`);
        } else {
            bot.sendMessage(chatId, 'Паролі не знайдено.');
        }
    } catch (error) {
        console.error('Error checking admin status or fetching last password:', error);
        bot.sendMessage(chatId, 'Помилка при перевірці статусу адміністратора або пошуку останнього пароля.');
    }
});

async function showPassword(chatId, user) {
    try {
        // const user = await User.findOne({ userId });
        if (!user || !user.isAdmin) {
            bot.sendMessage(chatId, 'Ви не є адміністратором. Доступ заборонено.');
            return;
        }

        const lastPasswordEntry = await Password.findOne().sort({ createdAt: -1 });

        if (lastPasswordEntry) {
            bot.sendMessage(chatId, `Останній збережений пароль: ${lastPasswordEntry.password}`);
        } else {
            bot.sendMessage(chatId, 'Паролі не знайдено.');
        }
    } catch (error) {
        console.error('Error checking admin status or fetching last password:', error);
        bot.sendMessage(chatId, 'Помилка при перевірці статусу адміністратора або пошуку останнього пароля.');
    }
}

bot.onText(/\/inputpassword/, async (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;

    try {
        const lastPasswordEntry = await Password.findOne().sort({ createdAt: -1 });

        if (!lastPasswordEntry) {
            bot.sendMessage(chatId, 'Паролі не знайдено.');
            return;
        }

        bot.sendMessage(chatId, 'Введіть пароль:')

        const inputPasswordHandler = async (msg) => {
            const enteredPassword = msg.text;
            const issetView = await View.findOne({ userId, date: new Date().toLocaleDateString('en-GB') })
            console.log(issetView);
            if (enteredPassword == lastPasswordEntry.password) {
                if (!issetView) {
                    const view = new View(
                        {
                            userId,
                        });
                    await view.save();
                    bot.sendMessage(chatId, `Пароль правильний. Ваш лічильник збільшено: `);
                } else {
                    bot.sendMessage(chatId, 'Ви вже вводили пароль.');
                }
            } else {
                bot.sendMessage(chatId, 'Невірний пароль.');
            }
            bot.off('text', inputPasswordHandler)
        }

        bot.on('text', inputPasswordHandler)
    } catch (error) {
        console.error('Error checking last password or updating counter:', error);
        bot.sendMessage(chatId, 'Відбулася помилка під час перевірки останнього пароля або оновлення лічильника.');
    }
});

async function inputPassword(chatId, user) {
    try {

        const lastPasswordEntry = await Password.findOne().sort({ createdAt: -1 });

        if (!lastPasswordEntry) {
            bot.sendMessage(chatId, 'Паролі не знайдено.');
            return;
        }

        bot.sendMessage(chatId, 'Введіть пароль:').then(() => {
            bot.on('text', async (msg) => {
                const enteredPassword = msg.text;
                const issetView = await View.find({ userId: user.userId, date: new Date().toLocaleDateString('en-GB') })
                if (enteredPassword === lastPasswordEntry.password
                    && !issetView) {
                    const view = new View(
                        {
                            userId: user.userId,
                        });
                    await view.save();
                    bot.sendMessage(chatId, `Пароль правильний. Ваш лічильник збільшено: `);
                } else {
                    bot.sendMessage(chatId, 'Невірний пароль.');
                }
            });
        });
    } catch (error) {
        console.error('Error checking last password or updating counter:', error);
        bot.sendMessage(chatId, 'Відбулася помилка під час перевірки останнього пароля або оновлення лічильника.');
    }
}

bot.on('callback_query', (callbackQuery) => {
    // Обработка нажатий на кнопки
});

console.log('Bot is running...');
console.log(new Date().toLocaleDateString('en-GB'))