const mongoose = require('mongoose');

const viewSchema = new mongoose.Schema({
    userId: { type: Number },
    date: { type: String, default: () => new Date().toLocaleDateString('en-GB') },
});

const View = mongoose.model('View', viewSchema);

module.exports = View