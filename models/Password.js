const mongoose = require('mongoose');

const passwordSchema = new mongoose.Schema({
    userId: { type: Number },
    password: String,
    createdAt: { type: Date, default: Date.now },
});

const Password = mongoose.model('Password', passwordSchema);

module.exports = Password